package com.example.mathieu.projetihm.ui

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.mathieu.projetihm.R
import com.example.mathieu.projetihm.state.State
import com.example.mathieu.projetihm.ui.grid.ImageGridFragment
import com.example.mathieu.projetihm.ui.image.ImageFragment
import kotlinx.android.synthetic.main.activity_images_list.*
import java.util.*
import kotlin.concurrent.fixedRateTimer

const val START = "Start"
const val PAUSE = "Pause"

// Vitesses
enum class Speed(val value: Long) {
    ONE(3_000),
    TWO(2_000),
    THREE(1_000)
}

class MainActivity : AppCompatActivity(), ImageGridFragment.ImageGridListener {

    // region Attributs

    // Par défaut, la première image est selectionnée
    private var selectedIndex = 0
    private var imageFragment: ImageFragment? = null
    private var gridFragment: ImageGridFragment? = null


    // Id des images
    private val images = intArrayOf(
        R.drawable.waterfall,
        R.drawable.tunisie,
        R.drawable.falaise,
        R.drawable.lac,
        R.drawable.mordor,
        R.drawable.dolmen,
        R.drawable.neige,
        R.drawable.tatooine,
        R.drawable.foret
    )

    // Ne calculer le nombre d'images qu'une seule fois puisqu'il ne peut pas changer
    // (meme si les appels à .size sont propablemenet déjà optimisés par la JVM pour
    // ne pas reparcourir le tableau
    private val imagesSize = images.size

    // Moment ou l'image a été clickée
    private var clickedTimeStamp: Long? = null

    // Etat courant, Vignettes au début
    private var state: State = State.V

    private var timer: Timer? = null

    // endregion

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_images_list)
        // Au début afficher la grille d'images
        showGrid()
        setUpListeners()
    }

    private fun setUpListeners() {
        btnStop.setOnClickListener { stop() }
        btnPrev.setOnClickListener { decreaseSpeed() }
        btnSuiv.setOnClickListener { increaseSpeed() }
        btnStart.setOnClickListener { handleStartClick() }
    }

    // region Méthodes utilitaires

    /**
     * Initialise le fragment contenant
     * la grille d'images
     */
    private fun setUpGrid(): Fragment {
        gridFragment = ImageGridFragment()
        with(Bundle()) {
            putIntArray("images", images)
            gridFragment!!.arguments = this
        }
        return gridFragment!!
    }

    /* Rend cliquables les boutons de changement de direction, pause & stop */
    private fun setNavigation(enabled: Boolean) {
        listOf(btnPrev, btnStart, btnSuiv, btnStop).forEach { it.isActivated = enabled }
    }

    /**
     * Change le texte du bouton start
     */
    private fun setStartButtonText(text: String) {
        btnStart.text = text
    }

    /**
     * Affiche la grille des images
     */
    private fun showGrid() {
        setUpGrid().also {
            supportFragmentManager
                .beginTransaction()
                .add(frame.id, it)
                .commit()
        }
    }

    /**
     * Lance le diaporama à partir de l'image
     * selectionnée en basculant sur le fragment image
     */
    private fun startDiapo() {
        setUpDiapo().also {
            supportFragmentManager
                .beginTransaction()
                .replace(frame.id, it)
                .commit()
        }
    }

    /**
     * Prépare le fragment diapo
     */
    private fun setUpDiapo(): Fragment {
        imageFragment = ImageFragment()
        with(Bundle()) {
            putInt("image", images[selectedIndex])
            imageFragment!!.arguments = this
        }
        return imageFragment!!
    }

    /**
     * Affiche l'image suivante dans le tableau
     */
    private fun nextImage() {
        imageFragment?.showImage(
            images[++selectedIndex % imagesSize]
        )
    }

    /**
     * Affiche l'image précédente dans le tableau
     */
    private fun previousImage() {
        // On utilise floorMod pour que le modulo gère correctement les valeurs
        // négatives
        imageFragment?.showImage(
            images[Math.floorMod(--selectedIndex, imagesSize)]
        )
    }

    /**
     * Change la vitesse du timer
     */
    private fun setSpeed(speed: Speed) {
        // Annuler le timer précédent si il existe
        timer?.cancel()
        timer?.purge()
        timer = fixedRateTimer(
            initialDelay = speed.value,
            period = speed.value,
            action = {
                runOnUiThread { onTick() }
            }
        )
    }

    private fun selectImage(index: Int) {
        selectedIndex = index
        clickedTimeStamp = System.currentTimeMillis()
    }

    // end region

    // region Changement etat

    /**
     * Appelée quand une image est cliquée dans la grille
     */
    override fun onImageClick(index: Int) {
        when (state) {
            // Si on est dans l'état initial, on selectionne l'image
            State.V -> {
                selectImage(index)
                state = State.IS
            }
            State.IS -> {
                if (index != selectedIndex) {
                    selectImage(index)
                    return
                }
                // Si ce n'est pas un double clic, rien à faire
                if (System.currentTimeMillis() - clickedTimeStamp!! > 5_000) return
                startDiapo()
                setSpeed(Speed.ONE)
                setNavigation(true)
                setStartButtonText(PAUSE)
                state = State.DS1
            }
            // Dans tous les autres états, un click sur une image ne fait rien
            else -> return
        }
    }

    /**
     * Appelée à chaque tick du timer
     */
    private fun onTick() {
        when (state) {
            State.DS1, State.DS2, State.DS3 -> nextImage()
            State.DP1, State.DP2, State.DP3 -> previousImage()
            else -> return
        }
    }

    private fun handleStartClick() {
        when (state) {
            State.V, State.IS -> {
                startDiapo()
                state = State.DS1
                setNavigation(true)
                setStartButtonText(PAUSE)
                setSpeed(Speed.ONE)
            }
            State.DS1 -> {
                pause()
                state = State.PDS1
            }
            State.DS2 -> {
                pause()
                state = State.PDS2
            }
            State.DS3 -> {
                pause()
                state = State.PDS3
            }
            State.DP1 -> {
                pause()
                state = State.PDP1
            }
            State.DP2 -> {
                pause()
                state = State.PDP2
            }
            State.DP3 -> {
                pause()
                state = State.PDP3
            }
            State.PDS1 -> {
                resume(Speed.ONE)
                state = State.DS1
            }
            State.PDS2 -> {
                resume(Speed.TWO)
                state = State.DS2
            }
            State.PDS3 -> {
                resume(Speed.THREE)
                state = State.DS3
            }
            State.PDP1 -> {
                resume(Speed.ONE)
                state = State.DS1
            }
            State.PDP2 -> {
                resume(Speed.TWO)
                state = State.DS2
            }
            State.PDP3 -> {
                resume(Speed.THREE)
                state = State.DS3
            }
        }
    }

    private fun pause() {
        setStartButtonText(START)
        timer?.cancel()
    }

    private fun resume(speed: Speed) {
        setStartButtonText(PAUSE)
        setSpeed(speed)
    }

    private fun increaseSpeed() {
        when (state) {
            State.DS1 -> {
                setSpeed(Speed.TWO)
                state = State.DS2
            }
            State.DS2 -> {
                setSpeed(Speed.THREE)
                state = State.DS3
            }
            State.DP3 -> {
                setSpeed(Speed.TWO)
                state = State.DP2
            }
            State.DP2 -> {
                setSpeed(Speed.ONE)
                state = State.DP1
            }
            State.DP1 -> {
                setSpeed(Speed.ONE)
                state = State.DS1
            }
            else -> return
        }
    }

    private fun decreaseSpeed() {
        when (state) {
            State.DP1 -> {
                setSpeed(Speed.TWO)
                state = State.DP2
            }
            State.DP2 -> {
                setSpeed(Speed.THREE)
                state = State.DP3
            }
            State.DS3 -> {
                setSpeed(Speed.TWO)
                state = State.DS2
            }
            State.DS2 -> {
                setSpeed(Speed.ONE)
                state = State.DS1
            }
            State.DS1 -> {
                setSpeed(Speed.ONE)
                state = State.DP1
            }
            else -> return
        }
    }

    private fun stop() {
        when (state) {
            State.V -> return
            else -> {
                timer?.cancel()
                state = State.V
                setStartButtonText(START)
                selectedIndex = 0
                supportFragmentManager
                    .beginTransaction()
                    .replace(frame.id, gridFragment!!)
                    .commit()
            }
        }
    }

    // end region

}
