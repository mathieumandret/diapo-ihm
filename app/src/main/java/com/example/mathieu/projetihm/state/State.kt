package com.example.mathieu.projetihm.state

enum class State {
    V, // Vignettes
    IS, // Image selectionnée
    DS1, // Defilement vers l'avant, 3s entre les images
    DS2, // Defilement vers l'avant, 2s entre les images
    DS3, // Defilement vers l'avant, 1s entre les images
    PDS1, // Defilement vers l'arrière, 3s entre les images
    PDS2, // Defilement vers l'arrière, 2s entre les images
    PDS3, // Defilement vers l'arrière, 1s entre les images
    DP1, // Pause vitesse 1 (3s) vers l'avant
    DP2, // Pause vitesse 2 (2s) vers l'avant
    DP3, // Pause vitesse 3 (2s) vers l'avant
    PDP1, // Pause vitesse 1 (3s) vers l'arrière
    PDP2, // Pause vitesse 2 (2s) vers l'arrière
    PDP3, // Pause vitesse 3 (3s) vers l'arrière
}