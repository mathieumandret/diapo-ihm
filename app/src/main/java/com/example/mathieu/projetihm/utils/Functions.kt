package com.example.mathieu.projetihm.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.widget.ImageView

fun createImageView(context: Context?, imageId: Int, scaleX: Int, scaleY: Int): ImageView =
    ImageView(context).also { img ->
        img.setImageBitmap(createBitmap(context, imageId, scaleX, scaleY))
    }

fun createBitmap(context: Context?, imageId: Int, scaleX: Int, scaleY: Int): Bitmap {
    val bitmap = (context?.getDrawable(imageId) as BitmapDrawable).bitmap
    return Bitmap.createScaledBitmap(bitmap, scaleX, scaleY, false)
}
