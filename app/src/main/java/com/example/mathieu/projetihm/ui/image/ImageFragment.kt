package com.example.mathieu.projetihm.ui.image

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.mathieu.projetihm.R
import com.example.mathieu.projetihm.utils.createBitmap
import kotlinx.android.synthetic.main.fragment_image.view.*


class ImageFragment : Fragment() {

    private lateinit var imageView: ImageView

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        /**
         * On passe la première image en argument parce qu'on ne peut pas appeler
         * showImage directemment car on doit attendre le onAttach pour avoir accès au contexte
         */
        val layout = inflater.inflate(R.layout.fragment_image, container, false)
        imageView = layout.image
        getImage().also { showImage(it) }
        return layout
    }

    private fun getImage(): Int =
        arguments?.getInt("image")
            ?: throw RuntimeException("Image must have an 'image' argument")

    /**
     * Affiche une image dans la view à partir de son ID
     */
    fun showImage(imageId: Int) {
        // L'afficher dans l'image
        imageView.setImageBitmap(
            createBitmap(
                context, imageId, 550, 500
            )
        )
    }
}
