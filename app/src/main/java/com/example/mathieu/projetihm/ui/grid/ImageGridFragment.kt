package com.example.mathieu.projetihm.ui.grid

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.example.mathieu.projetihm.R
import kotlinx.android.synthetic.main.fragment_image_grid.*

class ImageGridFragment : Fragment() {

    private var listener: ImageGridListener? = null
    private lateinit var images: Array<Int>


    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is ImageGridListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement ImageGridListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        getImages()
        return inflater.inflate(R.layout.fragment_image_grid, container, false)
    }

    private fun getImages() {
        // Recupere la liste des IDs d'images passée par l'activité
        images = arguments?.getIntArray("images")?.toTypedArray() ?:
                throw RuntimeException("ImageGrid must have an 'images' argument")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = ImageGridAdapter(images, context)
        grid.adapter = adapter
        grid.setOnItemClickListener { _, _, clickedIndex, _ ->
            selectImage(clickedIndex)
            images.indices
                .filter { it != clickedIndex }
                .forEach { unselectedImage(it) }
        }
    }

    fun selectImage(atIndex: Int) {
        drawBackground(grid.getItemAtPosition(atIndex) as View)
        // Prevenir l'activité qu'une image a été cliquée
        listener?.onImageClick(atIndex)
    }

    fun unselectedImage(atIndex: Int) = clearBackground(grid.getItemAtPosition(atIndex) as View)

    private fun drawBackground(v: View) {
        val padding = 10
        v.setPadding(padding, padding, padding, padding)
        v.background = ContextCompat.getDrawable(context!!, R.drawable.rectangle)
    }

    private fun clearBackground(v: View) {
        v.setPadding(0, 0, 0, 0)
        (v as ImageView).setBackgroundResource(android.R.color.transparent)
    }

    interface ImageGridListener {
        fun onImageClick(index: Int)
    }
}
