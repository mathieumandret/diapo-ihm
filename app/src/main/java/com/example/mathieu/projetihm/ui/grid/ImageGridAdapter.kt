package com.example.mathieu.projetihm.ui.grid

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.example.mathieu.projetihm.utils.createImageView

class ImageGridAdapter(ids: Array<Int>, ctx: Context?) : BaseAdapter() {

    private val imageIds: Array<Int> = ids
    private val images: List<ImageView>
    private val context: Context? = ctx

    init {
        // Transforme la liste d'id en liste d'images
        images = imageIds.map { createImageView(context, it, 350, 300) }
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View = images[position]

    override fun getItem(position: Int): Any = images[position]

    override fun getItemId(position: Int): Long = imageIds[position].toLong()

    override fun getCount(): Int = imageIds.size
}